WITH StoreRevenue AS (
    SELECT
        s.store_id,
        s.staff_id,
        s.first_name,
        s.last_name,
        SUM(p.amount) AS total_revenue
    FROM
        staff s
    INNER JOIN payment p ON s.staff_id = p.staff_id
    INNER JOIN rental r ON p.rental_id = r.rental_id
    WHERE
        TO_CHAR(r.rental_date, 'YYYY') = '2017'
    GROUP BY
        s.store_id, s.staff_id, s.first_name, s.last_name
),
RankStoreRevenue AS (
    SELECT
        store_id,
        staff_id,
        first_name,
        last_name,
        total_revenue,
        DENSE_RANK() OVER (PARTITION BY store_id ORDER BY total_revenue DESC) AS rank
    FROM
        StoreRevenue
)
SELECT
    store_id,
    staff_id,
    first_name,
    last_name,
    total_revenue
FROM
    RankStoreRevenue
WHERE
    rank = 1;
