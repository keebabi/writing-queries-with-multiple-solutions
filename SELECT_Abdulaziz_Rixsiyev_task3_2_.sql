WITH ActorFilmYears AS (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        MIN(f.release_year) AS earliest_film_year,
        MAX(f.release_year) AS latest_film_year
    FROM
        actor a
    LEFT JOIN film_actor fa ON a.actor_id = fa.actor_id
    LEFT JOIN film f ON fa.film_id = f.film_id
    GROUP BY
        a.actor_id, a.first_name, a.last_name
)
SELECT
    afy.actor_id,
    afy.first_name,
    afy.last_name,
    afy.earliest_film_year,
    afy.latest_film_year,
    COALESCE(afy.latest_film_year - afy.earliest_film_year, 0) AS inactivity_duration
FROM
    ActorFilmYears afy
ORDER BY
    inactivity_duration DESC;
