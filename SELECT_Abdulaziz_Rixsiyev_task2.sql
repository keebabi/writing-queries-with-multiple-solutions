WITH TopRentedMovies AS (
    SELECT
        f.film_id,
        f.title AS movie_title,
        COUNT(r.rental_id) AS rental_count
    FROM
        film f
    JOIN inventory i ON f.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        f.film_id, f.title
    ORDER BY
        rental_count DESC
    LIMIT 5
)
SELECT
    tm.movie_title,
    tm.rental_count
FROM
    TopRentedMovies tm;