WITH ActorInactivity AS (
    SELECT
        fa.actor_id,
        a.first_name,
        a.last_name,
        MIN(f.release_year) AS first_movie_year,
        MAX(f.release_year) AS last_movie_year
    FROM
        film_actor fa
    JOIN film f ON fa.film_id = f.film_id
    JOIN actor a ON fa.actor_id = a.actor_id
    GROUP BY
        fa.actor_id, a.first_name, a.last_name
)
SELECT
    ai.actor_id,
    ai.first_name,
    ai.last_name,
    ai.first_movie_year,
    ai.last_movie_year,
    (ai.last_movie_year - ai.first_movie_year) AS inactivity_duration
FROM
    ActorInactivity ai
ORDER BY
    inactivity_duration DESC;
